﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Opening : MonoBehaviour
{
    public string nextArea;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TitleCard());
    }

    IEnumerator TitleCard()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(nextArea);
    }
}

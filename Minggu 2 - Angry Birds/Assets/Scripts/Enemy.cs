﻿using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    public float Health = 50f;
    public UnityAction<GameObject> OnEnemyDestroyed = delegate { };
    public AudioSource enemyDeathSfx;

    void OnDestroy()
    {
        OnEnemyDestroyed(gameObject);
     }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;
        if (col.gameObject.tag == "Bird")
        {
            EnemyDefeat();
        }
        else if(col.gameObject.tag == "Obstacle")
        {
            //Hitung damage yang diperoleh.
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            Health -= damage;

            if(Health <= 0)
            {
                EnemyDefeat();
            }
        }
    }

    void EnemyDefeat()
    {
        GetComponent<Renderer>().enabled = false;
        transform.position = new Vector2(30, 30);
        enemyDeathSfx.Play(0);
        Destroy(gameObject, 1f);
    }
}

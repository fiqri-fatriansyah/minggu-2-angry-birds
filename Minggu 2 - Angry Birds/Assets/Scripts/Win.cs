﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WinCard());
    }

    IEnumerator WinCard()
    {
        yield return new WaitForSeconds(3);
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackBird : Bird
{
    [SerializeField]
    public float _boomForce = 500;
    public CircleCollider2D BlastRadius;
    private bool _isExplode = false;

    new void Start()
    {
        base.Start();
        BlastRadius.GetComponent<CircleCollider2D>().enabled = false;
    }
    
    void OnCollisionEnter2D(Collision2D col)
    {
        if ((col.gameObject.tag == "Enemy" || col.gameObject.tag == "Obstacle") && !_isExplode)
        {
            _isExplode = true;
            BlastRadius.GetComponent<CircleCollider2D>().enabled = true;
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if ((col.gameObject.tag == "Enemy" || col.gameObject.tag == "Obstacle") && !_isExplode)
        {
            _isExplode = true;
            BlastRadius.GetComponent<CircleCollider2D>().enabled = true;
        }
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastRadius : MonoBehaviour
{
    public BlackBird bird;
    private bool isBirdDestroyed;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy" || col.gameObject.tag == "Obstacle")
        {
            Vector2 direction = col.gameObject.transform.position + bird.transform.position;
            col.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(bird._boomForce / direction.x, bird._boomForce / direction.y));
            
            if (!isBirdDestroyed)
            {
                isBirdDestroyed = true;
                StartCoroutine(DestroyBird());
            }
        }
    }
    IEnumerator DestroyBird()
    {
        bird.gameObject.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.032f);
        bird.transform.position = new Vector2(40, 40);
        bird.gameObject.GetComponent<AudioSource>().Play(0);
        Destroy(bird.gameObject, 1f);
    }
}

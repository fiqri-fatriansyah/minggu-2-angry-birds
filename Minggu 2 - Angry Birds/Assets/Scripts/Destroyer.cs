﻿using UnityEngine;

public class Destroyer : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        string tag = col.gameObject.tag;
        if(tag == "Bird" ||  tag == "obstacle")
        {
            Destroy(col.gameObject);
        }
        else if(tag == "Enemy")
        {
            col.gameObject.GetComponent<AudioSource>().Play();
            col.gameObject.transform.position = new Vector2(30, 30);
            Destroy(col.gameObject, 1f);
        }
    }
}

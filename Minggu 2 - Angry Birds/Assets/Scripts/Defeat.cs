﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Defeat : MonoBehaviour
{
    public string nextArea;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DefeatCard());
    }

    IEnumerator DefeatCard()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(nextArea);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : MonoBehaviour
{
    public AudioSource woodCreakSfx;
    public AudioSource woodDestroyedSfx;

    public float Health = 30f;

    void OnCollisionEnter2D(Collision2D col)
    {
        string tag = col.gameObject.tag;
        if(tag == "Bird" || tag == "Enemy")
        {
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            Health -= damage;

            if (Health <= 0)
            {
                woodDestroyedSfx.Play(0);
                GetComponent<Renderer>().enabled = false;
                transform.position = new Vector2(-30, -30);
                Destroy(gameObject, 1f);
            }
            else
            {
                woodCreakSfx.Play(0);
            }
        }
    }
}
